import 'package:flutter/material.dart';

class SignInButtonBuilder extends StatelessWidget {
  final IconData icon;
  final Color backgroundColor;
  final String text;
  final Function onPressed;

  const SignInButtonBuilder(
      {Key key, this.icon, this.backgroundColor, this.text, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      onTap: () => onPressed(),
      tileColor: backgroundColor,
      title: Text(text),
    );
  }
}

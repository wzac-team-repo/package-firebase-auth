import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './model/user.dart';
import '../services/firestore_service.dart';
import '../services/firebase_auth_service.dart';

class FireStore extends StatefulWidget {
  @override
  _FireStoreState createState() => _FireStoreState();
}

class _FireStoreState extends State<FireStore> {
  FirestoreService _firestoreService;
  FirestoreUserService _firestoreUserService;
  FirebaseAuthService _firebaseAuthService;
  Stream<QuerySnapshot> _streamUsersSnapshot;

  var _selectedUid;

  @override
  void initState() {
    super.initState();
    _firebaseAuthService = FirebaseAuthService();
    _firestoreService = FirestoreService();
    _streamUsersSnapshot = _firestoreService.streamSnapshot('users');

    _firestoreUserService = FirestoreUserService(
      uid: _firebaseAuthService.instance.currentUser.email,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(color: Colors.grey[100]),
        Text(
          'Firestore demo',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
        Divider(color: Colors.grey[100]),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            BtnBuilder(
              text: 'Add new',
              onTap: () {
                _firestoreUserService.add(
                    collection: 'configurations',
                    data: {'foo': 'bar', 'test': 'test2'},
                    onError: (error) {});
              },
            ),
            Opacity(
              opacity: _selectedUid == null ? 0.5 : 1,
              child: BtnBuilder(
                text: 'Delete $_selectedUid',
                onTap: () {
                  _firestoreUserService.delete(
                    collection: 'configurations',
                    docId: _selectedUid,
                  );
                },
              ),
            ),
            BtnBuilder(
              text: 'Update',
              onTap: () {
                _firestoreUserService.updateField(
                  collection: 'configurations',
                  docId: _selectedUid,
                  field: 'test',
                  newData: 1,
                );
              },
            ),
          ],
        ),

        // user info
        StreamBuilder<QuerySnapshot>(
          stream: _streamUsersSnapshot,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text("Loading");
            }

            return SizedBox(
              height: 200,
              child: ListView(
                children: User.parseFireUsers(snapshot).map((User user) {
                  return ListTile(
                    title: Text(user.email),
                    subtitle: Text(user.uid),
                  );
                }).toList(),
              ),
            );
          },
        ),

        StreamBuilder<QuerySnapshot>(
            stream: _firestoreUserService.streamUserSnapshot(
              collection: 'configurations',
            ),
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text('Something went wrong');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return Text("Loading");
              }
              return Container(
                  color: Colors.blueAccent.shade700,
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    children: snapshot.data.docs.map((doc) {
                      var data = doc.data();
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            _selectedUid = doc.id;
                          });
                        },
                        child: ListTile(
                          leading: Icon(Icons.work),
                          title: Text(data.toString()),
                          subtitle: Text(doc.id),
                        ),
                      );
                    }).toList(),
                  ));
            })
      ],
    );
  }
}

class BtnBuilder extends StatelessWidget {
  final Function onTap;
  final String text;

  BtnBuilder({this.onTap, this.text});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () => onTap(), // needed
        child: Ink(
          padding: EdgeInsets.all(12),
          // use Ink
          width: 96,
          height: 96,
          color: Colors.blue,
          child: Center(
            child: Text(text, textAlign: TextAlign.center),
          ),
        ),
      ),
    );
  }
}

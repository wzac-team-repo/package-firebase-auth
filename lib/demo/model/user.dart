import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  final String email;
  final String uid;

  User({this.email, this.uid});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'] as String ?? 'empty',
      uid: json['uid'] as String ?? 'empty',
    );
  }

  static List<User> parseFireUsers(AsyncSnapshot<QuerySnapshot> snapshot) {
    final List<QueryDocumentSnapshot> _docs = snapshot.data.docs;
    return _docs
        .map((DocumentSnapshot doc) => User.fromJson(doc.data()))
        .toList();
  }
}

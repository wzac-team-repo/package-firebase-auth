import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../bloc/firebase_auth_bloc.dart';
import '../../services/firebase_auth_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../validator_service/lib/validator_service.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Registration'),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8),
          child: Column(
            children: [
              if (_message != null)
                Container(
                  margin: EdgeInsets.all(8.0),
                  padding: EdgeInsets.all(8.0),
                  color: _messageColor,
                  child: Text((_message).toString()),
                ),
              // BlocBuilder<FirebaseAuthBloc, FirebaseAuthState>(
              //   builder: (context, state) {
              //     LatestFirebaseAuthState _state = state;
              //     if (_state.user != null) {
              //       return Text(_state.user.email);
              //     } else {
              //       return Text('Not Logged-In');
              //     }
              //   },
              // ),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Email',
                ),
              ),
              TextField(
                controller: _passwordController,
                // obscureText: true,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Password',
                ),
              ),
              CupertinoButton(
                color: Colors.blue,
                onPressed: () => onRegister(),
                child: Builder(
                  builder: (context) {
                    if (_registering == null) {
                      return Text('Register');
                    }
                    if (_registering) {
                      return CupertinoActivityIndicator();
                    } else {
                      return Text('Completed');
                    }
                  },
                ),
              ),
              SizedBox(height: 24),
              CupertinoButton(
                color: Colors.blue,
                onPressed: () {
                  _authService.sendVerification();
                },
                child: Builder(
                  builder: (context) {
                    return Text('Resend verification link');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /**
   * ===== Logic =====
   */
  TextEditingController _emailController;
  TextEditingController _passwordController;
  String _email;
  String _password;
  String _message;
  Color _messageColor;
  bool _registering;
  List<String> _checkList;
  FirebaseAuthBloc _authBloc;
  LatestFirebaseAuthState _authState;
  FirebaseAuthService _authService;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<FirebaseAuthBloc>(context);
    _authState = _authBloc.state;
    _authService = FirebaseAuthService();

    _checkList = ['hasData', 'isEmail'];

    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    _emailController.addListener(() {
      _email = _emailController.text;
    });

    _passwordController.addListener(() {
      _password = _passwordController.text;
    });
  }

  void setMessage(newMessage, {color = Colors.red}) {
    print("SetMessage > $newMessage");
    setState(() {
      _message = newMessage;
      _messageColor = color;
    });
  }

  void isRegistering(bool value) {
    setState(() {
      _registering = value;
    });
  }

  void onRegister() async {
    ValidatorService.check(
      _email,
      _checkList,
      onError: (res) {
        setMessage('${res.errors.join('\n')}');
        return;
      },
      onSuccess: (res) async {
        // register
        isRegistering(true);
        await _authService.register(
          _email,
          _password,
          onError: (String message) => onRegisterError(message),
          onSuccess: (user) => onRegisterSuccess(user),
        );
        isRegistering(null);
      },
      debug: true,
    );

    return;
  }

  void onRegisterError(String message) {
    setMessage(message);
  }

  void onRegisterSuccess(user) async {
    await _authService.sendVerification();
    setMessage('A verification link has been sent to your email account.',
        color: Colors.amber);
    await _authService.signOut();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../services/firebase_auth_service.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Sign in'),
        ),
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 8),
          child: Column(
            children: [
              if (_message != null)
                Container(
                  margin: EdgeInsets.all(8.0),
                  padding: EdgeInsets.all(8.0),
                  color: _messageColor,
                  child: Text((_message).toString()),
                ),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Email',
                ),
              ),
              TextField(
                controller: _passwordController,
                // obscureText: true,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Password',
                ),
              ),
              CupertinoButton(
                color: Colors.blue,
                onPressed: () => onSignIn(),
                child: Text('Sign in'),
              )
            ],
          ),
        ),
      ),
    );
  }

  /**
   * ===== Logic =====
   */
  TextEditingController _emailController;
  TextEditingController _passwordController;
  String _email;
  String _password;
  String _message;
  Color _messageColor;
  FirebaseAuthService _firebaseAuthService;

  @override
  void initState() {
    super.initState();
    _firebaseAuthService = FirebaseAuthService();

    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    _emailController.addListener(() {
      _email = _emailController.text;
    });

    _passwordController.addListener(() {
      _password = _passwordController.text;
    });
  }

  void setMessage(newMessage, {color = Colors.red}) {
    print("SetMessage > $newMessage");
    setState(() {
      _message = newMessage;
      _messageColor = color;
    });
  }

  /**
   * Event:: when signin button is tapped
   */
  void onSignIn() async {
    _firebaseAuthService.signInWithEmail(
      _email,
      _password,
      onError: (message) => onSignInError(message),
      onSuccess: (user) => onSignInSuccess(user),
    );
  }

  /**
   * When signin has error
   */
  void onSignInError(message) {
    setMessage(message);
  }

  /**
   * when signin is success
   */
  void onSignInSuccess(user) {
    if (_firebaseAuthService.isVerified()) {
      setMessage('Signed-in', color: Colors.green);
      return;
    }
    setMessage(
      'Unverified - Please verify your email and try again.',
      color: Colors.amber,
    );
    _firebaseAuthService.signOut();
  }
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'pages/register_page.dart';
import 'pages/sign_in_page.dart';
import 'builders/sign_in_button_builder.dart';
import '../bloc/firebase_auth_bloc.dart';
import '../services/firebase_auth_service.dart';
import '../services/firebase_core_service.dart';
import 'firestore_view.dart';

class FirebaseAuthView extends StatelessWidget {
  // bloc provider
  FirebaseAuthBloc _firebaseBlocProvider(context) {
    FirebaseAuthBloc _firebaseAuthBloc =
        FirebaseAuthBloc(FirebaseAuthService());
    _firebaseAuthBloc.add(FirebaseAuthStarted());
    return _firebaseAuthBloc;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _firebaseBlocProvider(context),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Firebase Example App',
        theme: ThemeData.dark(),
        home: Scaffold(
          body: AuthTypeSelector(),
        ),
      ),
    );
  }
}

class AuthTypeSelector extends StatefulWidget {
  @override
  _AuthTypeSelectorState createState() => _AuthTypeSelectorState();
}

class _AuthTypeSelectorState extends State<AuthTypeSelector> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase Example App"),
      ),
      body: Container(
        child: _buildView(),
      ),
    );
  }

  /**
   * ========== Business Logic ===========
   */
  bool _firebaseInitialised = false;
  bool _firebaseError = false;

  FirebaseCoreService _firebaseCoreService;
  FirebaseAuthService _firebaseAuthService;

  String _status;

  @override
  void initState() {
    super.initState();
    setupServices();
  }

  /**
   * ========= Setup all services ==========
   */
  setupServices() async {
    await setupFirebaseCore();
    await setupFirebaseAuth();
  }

  /**
   * Setup Firebase Core
   */
  setupFirebaseCore() async {
    _firebaseCoreService = FirebaseCoreService();
    await _firebaseCoreService.init(onError: () => onFirebaseCoreInitError());
  }

  /**
   * Setup Firebase Authentication
   */
  setupFirebaseAuth() async {
    if (_firebaseError == true) return;

    _firebaseAuthService = FirebaseAuthService();

    setState(() {
      _firebaseInitialised = true;
    });
  }

  /**
   * ========= Events Handlers ==========
   */
  /**
   * when firebase core initalised error
   */
  onFirebaseCoreInitError() {
    setState(() {
      _firebaseError = true;
    });
  }

  /**
   * ========= builders ==========
   */
  _buildView() {
    return _firebaseError
        ? _buildFirebaseError()
        : _firebaseInitialised
            ? _buildFirebaseReady()
            : _buildFirebaseLoading();
  }

  _buildFirebaseError() {
    return Container(color: Colors.red, child: Text('error'));
  }

  _buildFirebaseLoading() {
    return CupertinoActivityIndicator();
  }

  _buildFirebaseReady() {
    return BlocBuilder<FirebaseAuthBloc, FirebaseAuthState>(
      builder: (context, state) {
        LatestFirebaseAuthState _state = state;

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (_firebaseAuthService.isNotLogged())
              Container(
                child: SignInButtonBuilder(
                  icon: Icons.person_add,
                  backgroundColor: Colors.indigo,
                  text: 'Registration',
                  onPressed: () => _pushPage(context, RegisterPage()),
                ),
                padding: const EdgeInsets.all(16),
                alignment: Alignment.center,
              ),
            if (_firebaseAuthService.isNotLogged())
              Container(
                child: SignInButtonBuilder(
                  icon: Icons.verified_user,
                  backgroundColor: Colors.orange,
                  text: 'Sign In',
                  onPressed: () => _pushPage(context, SignInPage()),
                ),
                padding: const EdgeInsets.all(16),
                alignment: Alignment.center,
              ),
            if (_firebaseAuthService.isLogged())
              Center(
                child: Container(
                  color: Colors.black87,
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      if (_state.user != null)
                        Text(
                            "$_status - ${_state.user.email} is verified = ${_state.user.emailVerified}"),
                      ListTile(
                        leading: Icon(Icons.exit_to_app),
                        title: Text('Sign out'),
                        tileColor: Colors.blue,
                        onTap: () => _firebaseAuthService.signOut(),
                      )
                    ],
                  ),
                ),
              ),
            if (_firebaseAuthService.isLogged()) FireStore(),
          ],
        );
      },
    );
  }

  /**
   * ========= methods ==========
   */
  void _pushPage(BuildContext context, Widget page) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }
}

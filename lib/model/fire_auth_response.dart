import 'package:firebase_auth/firebase_auth.dart';

/**
 * A class of Firebase Authentication Response
 */
class FireAuthResponse {
  final User user;
  final String message;

  FireAuthResponse(this.user, this.message);
}

/**
 * More on firestore usage: https://firebase.flutter.dev/docs/firestore/usage
 * # Filtering: https://firebase.flutter.dev/docs/firestore/usage#filtering
 * # Limiting: https://firebase.flutter.dev/docs/firestore/usage#limiting
 * # Ordering: https://firebase.flutter.dev/docs/firestore/usage#ordering
 * # Start & End Cursors: https://firebase.flutter.dev/docs/firestore/usage#start--end-cursors
 **/
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FirestoreService {
  final FirebaseFirestore _firestore;

  FirestoreService({FirebaseFirestore firestore})
      : _firestore = firestore ?? FirebaseFirestore.instance;

  /**
   * Return the instance
   */
  FirebaseFirestore get instance => _firestore;

  /**
   * Return a stream of a collection snapshots
   */
  Stream<QuerySnapshot> streamSnapshot(String value) {
    CollectionReference cf = _firestore.collection(value);
    return cf.snapshots();
  }

  /**
   * one time read
   */
  Future<DocumentSnapshot> readOnce({String collection, String uid}) {
    return _firestore.collection(collection).doc(uid).get();
  }

  /**
   * Return a collection 
   */
  CollectionReference collection(String value) {
    CollectionReference cf = _firestore.collection(value);
    return cf;
  }

  /**
   * Add to collection
   */
  Future<void> insert({
    String collection,
    dynamic data,
    @required onSuccess(value),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .add(data)
        .then((value) => onSuccess(value))
        .catchError((error) => onError(error));
  }

  /**
   * Uniquely add to collection
   */
  Future<void> uInsert({
    String collection,
    dynamic uid,
    dynamic data,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid)
        .set(data)
        .then((value) => onSuccess())
        .catchError((error) => onError(error));
  }

  /**
   * Update collection
   * works with nested too: .update({'info.address.zipcode': 90210})
   */
  Future<void> update({
    String collection,
    dynamic uid,
    dynamic data,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid)
        .update(data)
        .then((value) => onSuccess()) // not tested
        .catchError((error) => onError(error));
  }

  /**
   * Delete a document
   */
  Future<void> delete({
    String collection,
    dynamic uid,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid)
        .delete()
        .then((value) => onSuccess()) // not tested
        .catchError((error) => onError(error));
  }

  /**
   * Delete a field inside a document
   */
  Future<void> deleteField({
    String collection,
    dynamic uid,
    dynamic field,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid)
        .update({'$field': FieldValue.delete()})
        .then((value) => onSuccess()) // not tested
        .catchError((error) => onError(error));
  }
}

/**
   * User-based collection
   */
class FirestoreUserService extends FirestoreService {
  final dynamic _uid;
  FirestoreUserService({dynamic uid}) : _uid = uid;
  /**
   * Add new user's collection
   */
  Future<void> add({
    String collection,
    dynamic uid,
    dynamic data,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid ?? _uid)
        .collection(collection)
        .add(data)
        .then((value) => onSuccess())
        .catchError((error) => onError(error));
  }

  Future<void> delete({
    String collection,
    dynamic uid,
    dynamic docId,
    dynamic data,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid ?? _uid)
        .collection(collection)
        .doc(docId)
        .delete()
        .then((value) => onSuccess())
        .catchError((error) => onError(error));
  }

  Future<void> updateField({
    String collection,
    dynamic uid,
    dynamic docId,
    dynamic field,
    dynamic newData,
    @required onSuccess(),
    @required onError(error),
  }) async {
    return _firestore
        .collection(collection)
        .doc(uid ?? _uid)
        .collection(collection)
        .doc(docId)
        .update({'$field': newData})
        .then((value) => onSuccess())
        .catchError((error) => onError(error));
  }

  /**
   * Return a stream of a collection snapshots
   */
  Stream<QuerySnapshot> streamUserSnapshot({String collection, dynamic uid}) {
    CollectionReference cf = _firestore
        .collection(collection)
        .doc(uid ?? _uid)
        .collection(collection);
    return cf.snapshots();
  }
}

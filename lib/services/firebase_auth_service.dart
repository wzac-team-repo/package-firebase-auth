import 'package:firebase_auth/firebase_auth.dart';
import '../model//fire_auth_response.dart';

class FirebaseAuthService {
  final FirebaseAuth _firebaseAuth;

  FirebaseAuthService({FirebaseAuth firebaseAuth})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance;

  /**
   * Get the auth instance
   */
  FirebaseAuth get instance => _firebaseAuth;

  /**
   * A stream that can be listened to authentication state change
   */
  Stream<User> get onAuthStateChanged {
    return _firebaseAuth.authStateChanges();
  }

  /**
   * Signout user
   */
  Future<void> signOut() async {
    _firebaseAuth.signOut();
  }

  /**
   * Sign in with email and password
   * @param { String } email
   * @param { String } password
   */
  Future<FireAuthResponse> signInWithEmail(
    email,
    password, {
    onSuccess(User user),
    onError(String message),
  }) async {
    FireAuthResponse _res;
    User _user;

    try {
      UserCredential _userCredential =
          (await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      ));
      _user = _userCredential.user;

      _res = FireAuthResponse(_user, 'ok');
    } on FirebaseException catch (e) {
      switch (e.code) {
        case 'user-not-found':
          _res = FireAuthResponse(null, 'No user found for that email.');
          break;
        case 'wrong-password':
          _res =
              FireAuthResponse(null, 'Wrong password provided for that user.');
          break;
        default:
          _res = FireAuthResponse(null, 'Oops.. something went wrong.');
      }
    } catch (e) {
      _res = FireAuthResponse(null, 'Oops.. something went wrong.');
    }

    if (_res.user == null) {
      onError(_res.message);
    } else {
      onSuccess(_res.user);
    }

    return _res;
  }

  /**
   * Create user with email and password
   * @param { String } email
   * @param { String } password
   */
  Future<FireAuthResponse> register(
    email,
    password, {
    onSuccess(User user),
    onError(String message),
  }) async {
    FireAuthResponse _res;

    try {
      UserCredential _userCredential =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      _res = FireAuthResponse(
        _userCredential.user,
        'OK',
      );
    } on FirebaseAuthException catch (e) {
      switch (e.code) {
        case 'weak-password':
          _res = FireAuthResponse(null, 'The password provided is too weak.');
          break;
        case 'email-already-in-use':
          _res = FireAuthResponse(null, 'The email is already in use.');
          break;
        case 'invalid-email':
          _res = FireAuthResponse(null, 'The email adress is invalid.');
          break;
        case 'too-many-requests':
          _res = FireAuthResponse(null, 'Please try again in an hour.');
          break;
        default:
          print(e.code);
          _res = FireAuthResponse(null, 'Oops.. something went wrong.');
      }
    } catch (e) {
      _res = FireAuthResponse(null, 'Oops.. something went wrong.');
    }

    if (_res.user == null) {
      onError(_res.message);
    } else {
      onSuccess(_res.user);
    }

    return _res;
  }

  /**
   * Check if user is verified
   */
  bool isVerified() {
    return _firebaseAuth.currentUser.emailVerified;
  }

  /**
   * Check if user islogged in
   */
  bool isLogged() {
    return _firebaseAuth.currentUser != null;
  }

  bool isNotLogged() {
    return _firebaseAuth.currentUser == null;
  }

  /**
   * Send a verification email to user
   */
  Future<void> sendVerification() async {
    _firebaseAuth.currentUser.sendEmailVerification();
  }

  /**
   * Send a password reset link to user
   */
  Future<void> forgotPassword(String email) async {
    _firebaseAuth.sendPasswordResetEmail(email: email);
  }
}

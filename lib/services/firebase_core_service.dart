import 'package:firebase_core/firebase_core.dart';

class FirebaseCoreService {
  FirebaseApp _firebaseApp;

  Future<int> init({onSuccess(), onError()}) async {
    try {
      _firebaseApp = await Firebase.initializeApp();
      print("[✓] FirebaseCoreService initialised");
      return 200;
    } catch (e) {
      return 404;
    }
  }
}

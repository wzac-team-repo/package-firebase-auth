import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../services/firebase_auth_service.dart';
import 'package:meta/meta.dart';

part 'firebase_auth_event.dart';
part 'firebase_auth_state.dart';

class FirebaseAuthBloc extends Bloc<FirebaseAuthEvent, FirebaseAuthState> {
  FirebaseAuthBloc(this._firebaseAuthService)
      : super(LatestFirebaseAuthState(null));

  final FirebaseAuthService _firebaseAuthService;
  StreamSubscription _subscription;

  @override
  Stream<FirebaseAuthState> mapEventToState(FirebaseAuthEvent event) async* {
    // === started
    if (event is FirebaseAuthStarted) {
      print("[✓] BlocEvent:: FirebaseAuthStarted");
      await _subscription?.cancel();
      _subscription = _firebaseAuthService.onAuthStateChanged.listen(
        (user) => add(UpdateFirebaseAuth(user)),
      );
    }

    // === ticked
    if (event is UpdateFirebaseAuth) {
      print("[✓] BlocEvent:: UpdateFirebaseAuth");
      yield LatestFirebaseAuthState(event.user);
    }
  }

  @override
  Future<void> close() {
    print("[✓] BlocEvent:: Stream subscription closed.");
    _subscription?.cancel();
    return super.close();
  }
}

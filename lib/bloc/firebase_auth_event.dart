part of 'firebase_auth_bloc.dart';

@immutable
abstract class FirebaseAuthEvent extends Equatable {
  const FirebaseAuthEvent();

  @override
  List<Object> get props => [];
}

class FirebaseAuthStarted extends FirebaseAuthEvent {}

class UpdateFirebaseAuth extends FirebaseAuthEvent {
  final User user;

  UpdateFirebaseAuth(this.user);

  @override
  List<Object> get props => [user];
}

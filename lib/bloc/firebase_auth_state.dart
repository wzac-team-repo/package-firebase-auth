part of 'firebase_auth_bloc.dart';

@immutable
abstract class FirebaseAuthState {
  const FirebaseAuthState();

  @override
  List<Object> get props => [];
}

class LatestFirebaseAuthState extends FirebaseAuthState {
  final User user;

  LatestFirebaseAuthState(this.user);

  @override
  List<Object> get props => [user];
}

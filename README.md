# Firebase Auth Service

A basic firebase auth using bloc pattern

## Context

- [Firebase Auth Service](#firebase-auth-service)
  - [Context](#context)
  - [Prerequisite](#prerequisite)
  - [Install](#install)
  - [Authentication methods supported](#authentication-methods-supported)
  - [Expected Usage](#expected-usage)
    - [Setup Firebase Core](#setup-firebase-core)
    - [Setup Firebase Authentication](#setup-firebase-authentication)
      - [Build view](#build-view)
      - [Initialize bloc provider](#initialize-bloc-provider)
      - [Listen to authentication changes](#listen-to-authentication-changes)
      - [Initialize FirebaseAuthService](#initialize-firebaseauthservice)
      - [Calling a method (eg. register)](#calling-a-method-eg-register)
      - [Calling a method (eg. sign-in)](#calling-a-method-eg-sign-in)
    - [Setup Cloud Firestore](#setup-cloud-firestore)
      - [FirestoreService](#firestoreservice)
      - [StreamBuilder](#streambuilder)
      - [add to collection](#add-to-collection)
      - [Parsing Stream Snapshot to User Model](#parsing-stream-snapshot-to-user-model)
        - [User Model](#user-model)
    - [Basic Firestore Rules](#basic-firestore-rules)

## Prerequisite

```yaml
# Bloc pattern
flutter_bloc: ^6.0.6

# Authentication
firebase_core: "0.5.0+1"
firebase_auth: ^0.18.1+2
equatable: ^1.2.5

# Cloud storage
cloud_firestore: ^0.14.1+3
```

## Install

- Go to your `services` folder
- git clone this repo `git clone https://ansoncwork@bitbucket.org/wzac-team-repo/package-firebase-auth.git`

## Authentication methods supported

- email and password only at the moment

## Expected Usage

### Setup Firebase Core

```dart
bool _firebaseInitialised = false;
bool _firebaseError = false;

FirebaseCoreService _firebaseCoreService;
// FirebaseAuthService _firebaseAuthService;

@override
void initState() {
  setupServices();
}

setupServices() async {
  await setupFirebaseCore();
  // await setupFirebaseAuth();
}

setupFirebaseCore() async {
    _firebaseCoreService = FirebaseCoreService;
    await _firebaseCoreService.init(onError: () => onFirebaseCoreInitError());
}

onFirebaseCoreInitError() {
  setState(() {
    _firebaseError = true;
  });
}
```

### Setup Firebase Authentication

```dart

FirebaseAuthService _firebaseAuthService;

setupServices() async {
  await setupFirebaseCore();
  await setupFirebaseAuth(); // <== add this
}

/**
* Setup Firebase Authentication
*/
setupFirebaseAuth() async {
  if (_firebaseError == true) return;

  _firebaseAuthService = FirebaseAuthService();

  setState(() {
    _firebaseInitialised = true;
  });
}
```

#### Build view

```dart
Container(
  child: _buildView(),
)

Widget _buildView() {
  return _firebaseError
      ? _buildFirebaseError()
      : _firebaseInitialised
          ? _buildFirebaseReady()
          : _buildFirebaseLoading();
}
```

#### Initialize bloc provider

```dart
// bloc provider
FirebaseAuthBloc _firebaseBlocProvider(context) {

  // pass the firebase auth service into the bloc
  FirebaseAuthBloc _firebaseAuthBloc =
      FirebaseAuthBloc(FirebaseAuthService());

  // call the bloc event to start the stream
  _firebaseAuthBloc.add(FirebaseAuthStarted());

  return _firebaseAuthBloc;
}

BlocProvider(
  create: (context) => _firebaseBlocProvider(context),
  child: ...
)
```

#### Listen to authentication changes

```dart
BlocBuilder<FirebaseAuthBloc, FirebaseAuthState>(
  builder: (context, state) {
    LatestFirebaseAuthState _state = state;
    if (_state.user != null) {
      return Text(_state.user.email + 'is Logged');
    } else {
      return Text('Not Logged-In');
    }
  },
),
```

#### Initialize FirebaseAuthService

```dart
FirebaseAuthService _authService;

@override
void initState(){
  _authService = FirebaseAuthService();
}
```

#### Calling a method (eg. register)

```dart
await _firebaseAuthService.register(
  _email,
  _password,
  onError: (String message) => onRegisterError(message),
  onSuccess: (user) => onRegisterSuccess(user),
);

/**
  Error handler
**/
void onRegisterError(String message) {
  setMessage(message);
}

/**
  Success handler
**/
void onRegisterSuccess(user) async {
  await _firebaseAuthService.sendVerification();
  setMessage('A verification link has been sent to your email account.',
      color: Colors.amber);
  await _firebaseAuthService.signOut();
}

```

#### Calling a method (eg. sign-in)

```dart

await _firebaseAuthService.signInWithEmail(
  _email,
  _password,
  onError: (message) => onSignInError(message),
  onSuccess: (user) => onSignInSuccess(user),
);

/**
 Error handler
**/
void onSignInError(message) {
  setMessage(message);
}

/**
  Success handler
**/
void onSignInSuccess(user) {
  if (_firebaseAuthService.isVerified()) {
    setMessage('Signed-in', color: Colors.green);
    return;
  }
  setMessage(
    'Unverified - Please verify your email and try again.',
    color: Colors.amber,
  );
  _firebaseAuthService.signOut();
}
```

### Setup Cloud Firestore

#### FirestoreService

```dart
  FirestoreService _firestoreService;
  Stream<QuerySnapshot> _streamUsersSnapshot;

  @override
  void initState() {

    _firestoreService = FirestoreService();
    _streamUsersSnapshot = _firestoreService.streamUserSnapshot('users');
  }
```

#### StreamBuilder

```dart
StreamBuilder<QuerySnapshot>(
  stream: _streamUsersSnapshot,
  builder:
      (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {

    if (snapshot.hasError) {
      return Text('Something went wrong');
    }

    if (snapshot.connectionState == ConnectionState.waiting) {
      return Text("Loading");
    }

    return ListView(
      children: snapshot.data.docs.map((DocumentSnapshot doc) {
        var data = doc.data();
        return ListTile(
          title: Text(data['email']),
          subtitle: Text(data['uid']),
        );
      }).toList(),
    );
  },
),
```

#### add to collection

```dart
_firestoreService.insert('test', {'email': 'foo@bar.com', 'uid': '123456'});
```

#### Parsing Stream Snapshot to User Model

```dart
ListView(
  children: User.parseFireUsers(snapshot).map((User user) {
    return ListTile(
      title: Text(user.email),
      subtitle: Text(user.uid),
    );
  }).toList(),
),
```

##### User Model

```dart
class User {
  final String email;
  final String uid;

  User({this.email, this.uid});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'] as String ?? 'empty',
      uid: json['uid'] as String ?? 'empty',
    );
  }

  static List<User> parseFireUsers(AsyncSnapshot<QuerySnapshot> snapshot) {
    final List<QueryDocumentSnapshot> _docs = snapshot.data.docs;
    return _docs
        .map((DocumentSnapshot doc) => User.fromJson(doc.data()))
        .toList();
  }
}
```

### Basic Firestore Rules

```js
rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if
          request.time < timestamp.date(2020, 11, 27);
    }
    
    // Allow only authenticated content owners access
    match /configurations/{email}/{documents=**} {
      allow read, write: if request.auth != null && request.auth.token.email == email
    }
  }
}
```